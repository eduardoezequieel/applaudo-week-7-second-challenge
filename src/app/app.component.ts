import { LoaderService } from './services/loader.service';
import { GithubService } from './services/github.service';
import { Component, OnInit } from '@angular/core';
import { ColorsService } from './services/colors.service';
import { ResponseError } from './interfaces/error';
import { GithubUser, GithubRepository } from './interfaces/github';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  githubUser!: GithubUser;
  githubRepositories!: GithubRepository[];
  stars = 0;

  constructor(
    public colorsService: ColorsService,
    private githubService: GithubService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.colorsService.initTheme();
  }

  searchUser(username: string): void {
    this.loaderService.requestOnProgress();
    this.githubService.getUser(username).subscribe({
      next: (response: GithubUser) => {
        this.githubUser = response;
        this.getRepositories(username);
      },
      error: (error: ResponseError) => {
        this.loaderService.finishedRequest();
        alert(error.message);
      },
    });
  }

  private getRepositories(username: string): void {
    this.githubService.getRepositories(username).subscribe({
      next: (response: GithubRepository[]) => {
        this.githubRepositories = response;
        this.calcStars();
        this.loaderService.finishedRequest();
      },
      error: (error: ResponseError) => {
        this.loaderService.finishedRequest();
        alert(error.message);
      },
    });
  }

  private calcStars(): void {
    this.stars = 0;

    this.githubRepositories.forEach((repository) => {
      this.stars = this.stars + repository.stargazers_count;
    });
  }
}
