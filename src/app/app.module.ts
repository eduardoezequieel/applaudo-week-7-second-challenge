import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { CounterComponent } from './components/counter/counter.component';
import { ProjectComponent } from './components/project/project.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderComponent } from './components/loader/loader.component';
import { TrimDescriptionPipe } from './pipes/trim-description.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SearchFormComponent,
    PersonalInfoComponent,
    CounterComponent,
    ProjectComponent,
    LoaderComponent,
    TrimDescriptionPipe,
  ],
  imports: [BrowserModule, HttpClientModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
