import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trimDescription',
})
export class TrimDescriptionPipe implements PipeTransform {
  transform(value: string): unknown {
    if (value.length > 165) {
      return value.substring(0, 163) + '...';
    }
    return value;
  }
}
