import { GithubRepository } from './../../interfaces/github';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent {
  @Input() githubRepository!: GithubRepository;
}
