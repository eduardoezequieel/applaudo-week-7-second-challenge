import { GithubUser } from './../../interfaces/github';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
})
export class PersonalInfoComponent {
  @Input() githubUser!: GithubUser;
  @Input() stars!: number;
}
