import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
  showSpinner = false;

  constructor(private loaderService: LoaderService) {}

  ngOnInit(): void {
    this.loaderService.getLoader().subscribe((value) => {
      this.showSpinner = value;
    });
  }
}
