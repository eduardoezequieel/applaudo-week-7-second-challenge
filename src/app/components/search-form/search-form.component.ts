import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent {
  @Output() submitUsername = new EventEmitter();

  form = this.fb.group({
    username: [
      '',
      Validators.pattern(/^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i),
    ],
  });
  constructor(public fb: FormBuilder) {}

  submit(): void {
    this.submitUsername.emit(this.form.value.username);
  }
}
