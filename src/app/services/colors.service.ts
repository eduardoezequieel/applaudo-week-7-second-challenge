import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ColorsService {
  actualTheme!: string | null;

  constructor() {}

  initTheme(): void {
    document.body.classList.add('no-transition');

    if (!this.getTheme()) {
      this.setTheme('light');
    } else {
      this.actualTheme = this.getTheme();
    }

    document.body.classList.add(this.actualTheme!);
    
    setTimeout(() => {
      document.body.classList.remove('no-transition');
    }, 300);
  }

  changeTheme(): void {
    if (this.actualTheme == 'light') {
      document.body.classList.remove('light');
      document.body.classList.add(this.setTheme('dark'));
    } else if (this.actualTheme == 'dark') {
      document.body.classList.remove('dark');
      document.body.classList.add(this.setTheme('light'));
    }
  }

  private getTheme(): string | null {
    return localStorage.getItem('theme');
  }

  private setTheme(theme: string): string {
    this.actualTheme = theme;
    localStorage.setItem('theme', theme);
    return theme;
  }
}
