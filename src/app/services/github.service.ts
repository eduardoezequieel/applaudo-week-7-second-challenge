import { GithubRepository } from './../interfaces/github';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError } from 'rxjs';
import { GithubUser } from '../interfaces/github';

@Injectable({
  providedIn: 'root',
})
export class GithubService {
  API_GITHUB = 'https://api.github.com';
  headers = new HttpHeaders().set(
    'authorization',
    'token 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'
  );

  constructor(private http: HttpClient) {}

  getUser(user: string): Observable<GithubUser> {
    return this.http
      .get<GithubUser>(`${this.API_GITHUB}/users/${user}`, {
        headers: this.headers,
      })
      .pipe(
        catchError((error: Response) => {
          if (error.status == 404) {
            throw { expected: true, message: 'User not found' };
          }
          throw { expected: false, message: 'Something wrong happened' };
        })
      );
  }

  getRepositories(user: string): Observable<GithubRepository[]> {
    return this.http
      .get<GithubRepository[]>(`${this.API_GITHUB}/users/${user}/repos`, {
        headers: this.headers,
      })
      .pipe(
        catchError((error: Response) => {
          if (error.status == 404) {
            throw { expected: true, message: 'User repositories not found' };
          }
          throw { expected: false, message: 'Something wrong happened' };
        })
      );
  }
}
